import { createStore, combineReducers } from "redux";
import taskEvent from "../components/task/taskEvent";

const appReducer = combineReducers({ 
    taskReducer: taskEvent
});

const store = createStore(
    appReducer
);

export default store;